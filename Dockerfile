FROM alpine:latest

RUN apk update && apk add --no-cache curl python2 tar mailcap

# Fetch  brat
RUN curl http://weaver.nlplab.org/~brat/releases/brat-v1.3_Crunchy_Frog.tar.gz | tar xvz

WORKDIR /brat-v1.3_Crunchy_Frog/

COPY config.py ./

RUN mkdir /data /work && ln -s /data . && ln -s /work .

EXPOSE 8080

CMD ["python2", "/brat-v1.3_Crunchy_Frog/standalone.py", "8080"]